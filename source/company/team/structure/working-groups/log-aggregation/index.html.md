---
layout: markdown_page
title: "Log Aggregation Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value |
|--------------|-------|
| Date Created | March 6, 2019 |
| Date Ended   | TBD |
| Slack        | [#wg_log-aggregation](https://gitlab.slack.com/messages/CGR0T1C6P) (only accessible from within the company) |
| Google Doc   | [Log Aggregation Working Group](https://docs.google.com/document/d/192B68tEuw5KoJEKwlzDlVbXS8PaxOT57M2MAcjCbHVo/edit) (only accessible from within the company) |

## Business Goal

Increase the quality, value, and accessibility of our GitLab.com logs.

Intent is to:

1. Analyze and document the locations, retention, and variety of production logs as they exist today, including analysis of the data classification and access controls
1. Develop troubleshooting and investigation guides to make best use of GitLab.com logs
1. Perform gap analysis of log quality and completeness, and where improvements can be made to streamline investigations
1. Work with Infra and Development teams to integrate the proposed improvements into GitLab.com

## Roles and Responsibilities

| Working Group Role    | Person                | Title                               |
|-----------------------|-----------------------|-------------------------------------|
| Facilitator           | Paul Harrison         | Senior Security Engineer            |
| Security Lead         | Kathy Wang            | Senior Director of Security         |
| Infrastructure Lead   | Vacancy               |                                     |
| Member                | Stan Hu               | Engineering Fellow                  |
| Member                | Antony Saba           | Senior Threat Intelligence Engineer |
| Member                | Andrew Newdigate      | Staff Engineer, Infrastructure      |
| Advisor               | Melissa Farber        | Security Manager, Compliance        |
