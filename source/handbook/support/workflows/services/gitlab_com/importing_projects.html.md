---
layout: markdown_page
title: Importing Projects for Customers
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

---

## Overview

This workflow is meant to provide guidance on when GitLab Team members might offer to import projects on behalf of customers or prospects as a courtesy, and the process for doing the imports.

Due to the shifting nature of what issues might be relevant, the specifics of this workflow may frequently change.

## Criteria

The Support Team can offer to import a few projects as a best effort courtesy, but anything complex with a hundreds of users or projects is out of scope. These users should be referred to [Professional Services](https://about.gitlab.com/services/migration/).

Additionally:

1. The requestor should be an existing customer or a prospect.
1. The import was attempted and failed one or more times.
1. The import method is a GitLab project export file.

### Pre-Approved Cases

Due to [CE issue #52956](https://gitlab.com/gitlab-org/gitlab-ce/issues/52956), users with large projects (~1GB+) will often see partial imports.

An admin is required to corrently map users in GitLab as [per our documentation](https://docs.gitlab.com/ee/user/project/settings/import_export.html). There is a [feature proposal to not require an admin user](https://gitlab.com/gitlab-org/gitlab-ce/issues/36338) being discussed.

When in doubt, file an issue in [dotcom-escalations](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues) and ask for a manager's input. If a manager approves, proceed with the import or if it requires console access, mark as `SE Escalation`.

## Process

### Verify Permissions

Before offering to do the import, verify the following:

1. User is a group owner.
1. User has rights to create new projects in the space.

If not verified, let the user know we might be able to offer an import, but a group owner needs to contact us or they need to adjust their settings (depending on which of the above is not verified).

### Gather Information and Export

Once verified:

1. Offer for the GitLab team to do an import on their behalf as a courtesy.
1. Ask the user to:
    1. confirm the group where the project should be imported.
    1. confirm no project currently exists in the namespace with the project's name.
    1. provide a copy of their project's export. Recommended sites: send.firefox.com (up to 1GB, expires after 24 hours), wetransfer.com (up to 2 GB).
    1. Depending on the case:
        1. _Correctly mapped users_: that _all_ users have accounts on GitLab.com with matching email address.
        1. _No user mapping_: provide the username for items to be attributed to.
1. When receiving the project export: download, and ensure it unpacks. If not, ask the user for another copy.
1. For correctly mapped users only:
    1. Pull out the email addresses in the export using something like `cat project.json | jq '.project_members[].user.email'`
    1. Verify that the email addresses all exist on GitLab.com and that they have permissions in the group level namespace the import is happening in. If not, respond to the customer with the list of email addresses not in GitLab.com.
    1. Keep a copy of this list for the `import` template below.
1. Assuming success, respond to the user that we have the export and will send an update when the import is complete.

### Review Export and Request Import

1. Fill in the `import` template on the [infra issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/new?issuable_template=import) with all necessary information including a link to the export and the ZD link for reference.
1. Delete any local copies of the export.
1. Fill out an `access request` template on the [access-requests tracker](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request) for admin level access to perform the import.
1. A message will be sent to #announcements channel on Slack when done.
1. Ensure the export file is deleted.
1. Let the user know the import is done and they should double check its completeness.
