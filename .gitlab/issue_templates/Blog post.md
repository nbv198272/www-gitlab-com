<!-- See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process -->

### Proposal

<!-- What do you want to blog about? Add your description here -->

### Checklist

- [ ] Due date and milestone added for the desired publish date
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts-official-announcements-company-updates-breaking-changes-and-news)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP

/label ~"blog post"
